namespace blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "Tittle", c => c.String());
            DropColumn("dbo.Posts", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Title", c => c.String());
            DropColumn("dbo.Posts", "Tittle");
        }
    }
}
